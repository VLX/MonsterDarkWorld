﻿#pragma strict
import System.Collections.Generic;

var allpokemon : Pokemon[];
var enemypokemon : Pokemon;
//var pokemonequipped : Pokemon;
var turn : int;

var otherp : PlayerStats;
var pokemonattacks : PokemonAttacks;

var pokemoninventory : Pokemon[];
var pokemononfield : int;

var cmenu : int;

function Awake(){
    otherp = gameObject.Find("Player").GetComponent(PlayerStats);
    pokemonattacks = gameObject.Find("Player").GetComponent(PokemonAttacks);
}

function Start () {
    copypokemon();
}

function OnGUI () {

    var other : PlayerStats;
    other = gameObject.Find("Player").GetComponent(PlayerStats);
    if(other.isInCombat){
        GUI.skin.label.fontSize = 30;
        GUI.Label (Rect(95,55,200,100), "" + enemypokemon.name);
        GUI.Label (Rect(370,125,200,100),enemypokemon.curhp + " / " + enemypokemon.basehp);
        GUI.DrawTexture (Rect(960,110,128,128), enemypokemon.image);

        GUI.Label (Rect(800,280,200,100), "" + pokemoninventory[pokemononfield].name);
        GUI.Label (Rect(1090,332,200,100), pokemoninventory[pokemononfield].curhp + " / " + pokemoninventory[pokemononfield].basehp);
        GUI.DrawTexture (Rect(310,290,128,128), pokemoninventory[pokemononfield].image);

    

        if(turn == 0){
            if(GUI.Button(Rect(800,435,200,60),"FIGHT")) cmenu = 1;
            if(GUI.Button(Rect(1100,435,200,60),"BAG")) {}
            if(GUI.Button(Rect(800,505,200,60),"POKEMON")){combatChangePokemon(1);}
            if(GUI.Button(Rect(1100,505,200,60),"RUN")) otherp.exitcombat();

            switch(cmenu){
                case 1:
                    if(GUI.Button(Rect(100,435,200,60),"" + pokemoninventory[pokemononfield].attacks[0].name)){
                        Debug.Log(pokemoninventory[pokemononfield].attacks[0].name);
                        pokemonattacks.useAbility("" + pokemoninventory[pokemononfield].attacks[0].name, turn, pokemoninventory[pokemononfield].attacks[0].type);
                        playerAttacked();
                        cmenu = 0;
                    }
                    if(GUI.Button(Rect(100,505,200,60),"" + pokemoninventory[pokemononfield].attacks[1].name)){
                        Debug.Log(pokemoninventory[pokemononfield].attacks[1].name);
                        pokemonattacks.useAbility("" + pokemoninventory[pokemononfield].attacks[1].name, turn, pokemoninventory[pokemononfield].attacks[1].type);
                        playerAttacked();
                        cmenu = 0;
                    }
                    if(GUI.Button(Rect(400,435,200,60),"" + pokemoninventory[pokemononfield].attacks[2].name)){
                        Debug.Log(pokemoninventory[pokemononfield].attacks[2].name);
                        pokemonattacks.useAbility("" + pokemoninventory[pokemononfield].attacks[2].name, turn, pokemoninventory[pokemononfield].attacks[2].type);
                        playerAttacked();
                        cmenu = 0;
                    }
                    if(GUI.Button(Rect(400,505,200,60),"" + pokemoninventory[pokemononfield].attacks[3].name)){
                        Debug.Log(pokemoninventory[pokemononfield].attacks[3].name);
                        pokemonattacks.useAbility("" + pokemoninventory[pokemononfield].attacks[3].name, turn, pokemoninventory[pokemononfield].attacks[3].type);
                        playerAttacked();
                        cmenu = 0;
                    }
                    
            }
            
        }
        
    }

}

function playerAttacked(){
    //Debug.Log("AAAAAAAA");
    turn = 1;
    enemyAttacked();
}

function enemyAttacked(){

    yield WaitForSeconds(2);
    if(otherp.isInCombat){
       // Debug.Log("OOOOOOOO");
        var randomattack = Random.Range(0,4);
       // Debug.Log(randomattack);
        pokemonattacks.useAbility("" + enemypokemon.attacks[randomattack].name, turn, enemypokemon.attacks[randomattack].type);
    }
   
    yield WaitForSeconds(2);
    turn = 0;
}


function Update () {
    if(otherp.isInCombat){
        if(pokemoninventory[pokemononfield].curhp <= 0){
            Debug.Log("DEEEEEAD");
            combatChangePokemon(0);
           // pokemonequipped.curhp = pokemonequipped.maxhp;
          // pokemonequipped.basehp = pokemonequipped.maxhp;
        }
        if(enemypokemon.curhp <= 0){
            Debug.Log("PSOFOS");
            otherp.exitcombat();
           enemypokemon.curhp = enemypokemon.maxhp;
           enemypokemon.basehp =  enemypokemon.maxhp;
        }
    }
  

}

function randomizepokemon(){
    
    var other : PlayerStats;
    other = gameObject.Find("Player").GetComponent(PlayerStats);

    var temppokemons : List.<Pokemon> = new List.<Pokemon>();
    var randomnum : int = Random.Range(0,100);

    if(randomnum == 20){
        for(var i=0; i<allpokemon.Length; i++){
            if(allpokemon[i].rarity == Rarity.rare && allpokemon[i].regionlocated == other.region){
                temppokemons.Add(allpokemon[i]);
            }
        }
    } else {
        for(var j=0; j<allpokemon.Length; j++){
            if(allpokemon[j].rarity == Rarity.common && allpokemon[j].regionlocated == other.region){
                temppokemons.Add(allpokemon[j]);
            }
        }
    }

    var newrandom = Random.Range(0, temppokemons.Count);
    enemypokemon = temppokemons[newrandom];
}

function combatChangePokemon(change : int){

    if(change == 0){
    for(var i=0; i<2; i++){
        if(pokemoninventory[i].curhp > 0){
            pokemononfield = i;
            return;
        }
      }
    }
    if(change == 1){
         for( i=0; i<3; i++){
             if(pokemoninventory[i].curhp > 0 && pokemoninventory[pokemononfield] == pokemoninventory[i]){
                 if(i<2)
                     if(pokemoninventory[i+1].curhp > 0)
                         pokemononfield = i+1;
                if(i==2 && pokemoninventory[0].curhp > 0)
                    pokemononfield = 0;
                 return;
             }
         }
    }
    otherp.exitcombat();
}

function copypokemon(){

    for(var i=0; i<2; i++){

        var pokemoninventory2 = new Pokemon();
        pokemoninventory2 = allpokemon[i];
     //   pokemoninventory[i] = pokemoninventory2;
    }
}

function heal(){
    for(var i=0; i<3; i++){
        pokemoninventory[i].curhp = pokemoninventory[i].maxhp;
    }
}