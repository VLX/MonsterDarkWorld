﻿#pragma strict

class Attack {
    var name : Move;
    var damage : float;
    var type : Type;
}


enum Move{
    thundershock,
    thunderball,
    razor_blades,
    water_gun,
    ember,
    earthquake,
    none
}
