﻿#pragma strict

//moving
var startpoint : Vector3;
var endpoint : Vector3;
var speed : float;
private var increment : float;
var isMoving : boolean;
var disablemovement : boolean;

//walking
var walkcounter : int;
var walkcounter2 : int;
var isInCombat : boolean;
var directionfacing : String;

//cameras
var CameraMain : GameObject;
var CombatCamera : GameObject;

//teleports
var player : GameObject;
var teleportLoc : GameObject[];

//regions
var region : String;

//call main
var mainscript : Head;

var audios :  AudioSource;

var battle:  AudioClip;
var city:  AudioClip;
var mewtwo:  AudioClip;
var route:  AudioClip;
var center:  AudioClip;
var cave:  AudioClip;


function Start () {
    region="region1";
    startpoint = transform.position;
    endpoint = transform.position;

    walkcounter2 = Random.Range(5,15);
    
    
}

function Update () {
    var Sprite = gameObject.GetComponent(AnimateSprite);

    if(Input.GetKeyDown("space") && !disablemovement){
        talkToNPC();
    }

    if(increment <= 1 && isMoving == true){
        increment += speed/100;
       // Debug.Log("Moving");
    } else {
        isMoving = false;
       // Debug.Log("Stopped");
    }

    if(isMoving)
        transform.position = Vector3.Lerp(startpoint, endpoint, increment);
    else
        Sprite.totalcells = 1;

    if(!isInCombat && !disablemovement){
        var disablemove : boolean;
        var hit : RaycastHit;
        var distanceToGround;
        if(Input.GetKey("w") && isMoving == false){       
            if(Physics.Raycast (transform.position, Vector3.forward, hit, 200.0)){
                Sprite.rownumber = 3;
                Sprite.totalcells = 1;
                directionfacing = "north";

                disablemove = false; 
                distanceToGround = hit.distance;
                Debug.Log("HIT");
    
                if (hit.collider.gameObject.tag == "Tree" || 
                    hit.collider.gameObject.tag == "NPC" || 
                    hit.collider.gameObject.tag == "Nurse" || 
                    hit.collider.gameObject.tag == "Mewtwo"){
                    disablemove = true;   
                  //  Debug.Log("Tree");
                }    
                if (hit.collider.gameObject.tag == "Entrance"){
                    this.transform.position = teleportLoc[0].transform.position;
                    this.transform.position.y += 10; 
                    region = "region3";
                    audios.clip = cave;
                    audios.Play();
                   // Debug.Log("entr");
                }  
                if (hit.collider.gameObject.tag == "CityEx"){
                    this.transform.position = teleportLoc[2].transform.position;
                    this.transform.position.y += 10; 
                    region = "region1";
                    audios.clip = route;
                    audios.Play();
                    // Debug.Log("entr");
                } 
                if (hit.collider.gameObject.tag == "CenterEn"){
                    this.transform.position = teleportLoc[5].transform.position;
                    this.transform.position.y += 10; 
                    region = "region5";
                    audios.clip = center;
                    audios.Play();
                    // Debug.Log("entr");
                } 
            }
         if(!disablemove){
            Sprite.rownumber = 3;
            Sprite.totalcells = 4;
            calculateWalk();
            increment = 0;
            isMoving = true;
            startpoint = transform.position;
            endpoint = new Vector3(transform.position.x, transform.position.y, transform.position.z + 200);
         }
        }

        if(Input.GetKey("s") && isMoving == false){
            if(Physics.Raycast (transform.position, -Vector3.forward, hit, 200.0)){
                Sprite.rownumber = 0;
                Sprite.totalcells = 1;
                directionfacing = "south";

                disablemove = false; 
                distanceToGround = hit.distance;
                Debug.Log("HIT");
    
                if (hit.collider.gameObject.tag == "Tree" || hit.collider.gameObject.tag == "NPC"){
                    disablemove = true;   
                    //  Debug.Log("Tree");
                }                    
                if (hit.collider.gameObject.tag == "Exit"){
                    this.transform.position = teleportLoc[1].transform.position;
                    this.transform.position.y += 10; 
                    region = "region1";
                    audios.clip = route;
                    audios.Play();
                   // Debug.Log("ex");
                } 
                if (hit.collider.gameObject.tag == "CityEn"){
                    this.transform.position = teleportLoc[3].transform.position;
                    this.transform.position.y += 10; 
                    region = "region4";
                    audios.clip = city;
                    audios.Play();
                    // Debug.Log("entr");
                } 
                if (hit.collider.gameObject.tag == "CenterEx"){
                    this.transform.position = teleportLoc[4].transform.position;
                    this.transform.position.y += 10; 
                    region = "region4";
                    audios.clip = city;
                    audios.Play();
                    // Debug.Log("entr");
                } 
            }
            if(!disablemove){
                Sprite.rownumber = 0;
                Sprite.totalcells = 4;
                calculateWalk();
                increment = 0;
                isMoving = true;
                startpoint = transform.position;
                endpoint = new Vector3(transform.position.x, transform.position.y, transform.position.z - 200);
            }
        }
        if(Input.GetKey("a") && isMoving == false){
            if(Physics.Raycast (transform.position, Vector3.left, hit, 200.0)){
                Sprite.rownumber = 1;
                Sprite.totalcells = 1;
                directionfacing = "west";

                disablemove = false; 
                distanceToGround = hit.distance;
               // Debug.Log(hit.collider.gameObject.tag);
    
                if (hit.collider.gameObject.tag == "Tree" || hit.collider.gameObject.tag == "NPC"){
                    disablemove = true;   
                      //Debug.Log("Tree");
                }           
            }
            if(!disablemove){
                Sprite.rownumber = 1;
                Sprite.totalcells = 4;
                calculateWalk();
                increment = 0;
                isMoving = true;
                startpoint = transform.position;
                endpoint = new Vector3(transform.position.x - 200, transform.position.y, transform.position.z);
            }
        }
        if(Input.GetKey("d") && isMoving == false){
            if(Physics.Raycast (transform.position, Vector3.right, hit, 200.0)){
                Sprite.rownumber = 2;
                Sprite.totalcells = 1;
                directionfacing = "east";

                disablemove = false; 
                distanceToGround = hit.distance;
               // Debug.Log("HIT");
    
                if (hit.collider.gameObject.tag == "Tree" || hit.collider.gameObject.tag == "NPC"){
                    disablemove = true;   
                    //  Debug.Log("Tree");
                } 
              
            }
            if(!disablemove){
                Sprite.rownumber = 2;
                Sprite.totalcells = 4;
                calculateWalk();
                increment = 0;
                isMoving = true;
                startpoint = transform.position;
                endpoint = new Vector3(transform.position.x + 200, transform.position.y, transform.position.z);
            }
        }
    }
}

function talkToNPC(){
        var hit : RaycastHit;
        if(Physics.Raycast (transform.position, Vector3.forward, hit, 200.0)){
            var dinstanceToGround = hit.distance;
            Debug.Log("npch");

            if(hit.collider.gameObject.tag == "NPC"){
                hit.collider.SendMessage("talk");
            }
            if(hit.collider.gameObject.tag == "Nurse"){
                hit.collider.SendMessage("talk");
                mainscript.heal();
            }
            if(hit.collider.gameObject.tag == "Mewtwo"){
                hit.collider.SendMessage("talk");
                audios.clip = mewtwo;
                audios.Play();
                entercombat();
                mainscript.enemypokemon = mainscript.allpokemon[5];

            }
        }
}



function calculateWalk(){

    yield WaitForSeconds(0.3);

    var hit : RaycastHit;
    
    if(Physics.Raycast (transform.position, -Vector3.up, hit, 100.0)){
        var distanceToGround = hit.distance;
       // Debug.Log("HIT");
    
        if (hit.collider.gameObject.tag == "Grass"){
            walkcounter++;      
           // Debug.Log("Grass");
        }
    }

        if(walkcounter >= walkcounter2){
            walkcounter2 = Random.Range(5,15);
            walkcounter = 0;
            audios.clip = battle;
            audios.Play();
            entercombat();
        } 
    }

function entercombat(){
   
        mainscript.randomizepokemon();
        CameraMain.active = false;
        CombatCamera.active = true;
        isInCombat = true;
        Debug.Log("Battole");
    }

function exitcombat(){
    if(region == "region3")
        audios.clip = cave;
    if(region == "region1" || region == "region2")
        audios.clip = route;
        audios.Play();
        CameraMain.active = true;
        CombatCamera.active = false;
        isInCombat = false;
        Debug.Log("Battole");
    }

    function OnTriggerEnter(col : Collider){

        if(col.gameObject.tag == "Region1"){
            region = "region1";
            Debug.Log("r1");
        }
        if(col.gameObject.tag == "Region2"){
            region = "region2";
            Debug.Log("r2");
        }
    }
