﻿#pragma strict

class Pokemon {

    var name : String;
    var image : Texture2D;
    var rarity : Rarity;
    var regionlocated : String;
    var type : Type;
    var weakness : Type;
    var basehp : float;
    var maxhp : float;
    var curhp : float;
    var baseatk : float;
    var curatk : float;
    var basedef : float;
    var curdef : float;
    var speed : float;
    var attacks : Attack[];
    }

    enum Type{
    electric,
    grass,
    water,
    fire,
    ground
    }

    enum Rarity{
      common,
      rare
    }