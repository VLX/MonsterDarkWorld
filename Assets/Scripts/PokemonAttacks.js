﻿#pragma strict

var mainscript : Head;

function Awake (){
    mainscript = gameObject.Find("Head").GetComponent(Head);
}

function Start () {

}

function Update () {

}

function useAbility(attackName : String, turn : int, type : Type){

    switch(attackName){
        case "thundershock": thundershock(turn,type); break;
        case "thunderball": thunderball(turn,type); break;
        case "razor_blades": razor_blades(turn,type); break;
        case "water_gun": water_gun(turn,type); break;
        case "ember": ember(turn,type); break;
        case "earthquake": ember(turn,type); break;

        default: none(turn,type); break;
    }

}



    function thundershock(turn : int, type : Type){
        Debug.Log("thundershock attack");
        calculatedamage(3,turn,type);
    }

        function thunderball(turn : int, type : Type){
        Debug.Log("thunderball attack");
        calculatedamage(4,turn,type);
        }
            function razor_blades(turn : int, type : Type){
                Debug.Log("razor_blades attack");
                calculatedamage(4,turn,type);
            }
                function water_gun(turn : int, type : Type){
                    Debug.Log("water_gun attack");
                    calculatedamage(3,turn,type);
                }
                    function ember(turn : int, type : Type){
                        Debug.Log("ember attack");
                        calculatedamage(2,turn,type);
                    }
                        function earthquake(turn : int, type : Type){
                            Debug.Log("earthquake attack");
                            calculatedamage(6,turn,type);
                        }

  function none(turn : int, type : Type){
   Debug.Log("none attack");
   calculatedamage(0,turn,type);
 }

      function calculatedamage(damage : int, turn : int, type : Type){
     

          if(turn == 0){
              if(mainscript.enemypokemon.weakness == type){
                  mainscript.enemypokemon.curhp -= damage*2;
              }else{
                  mainscript.enemypokemon.curhp -= damage;
              }
           
        } else {
              if(mainscript.pokemoninventory[mainscript.pokemononfield].weakness == type){
                  mainscript.pokemoninventory[mainscript.pokemononfield].curhp -= damage*2;
              }else{
                  mainscript.pokemoninventory[mainscript.pokemononfield].curhp -= damage;
              }
        }
 }