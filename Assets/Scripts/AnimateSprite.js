﻿#pragma strict

//whole picture
var colcount : int = 4;
var rowcount : int = 4;

//animation
var rownumber : int = 0;
var colnumber : int = 0;
var totalcells : int = 4;
var fps : int = 10;
var offset : Vector2;

function Update () {
    SetSpriteAnimation(colcount,rowcount,rownumber,colnumber,totalcells,fps);
}

function SetSpriteAnimation(colcount : int,rowcount: int,rownumber: int,colnumber: int,totalcells: int,fps: int){
    var index : int = Time.time * fps;
    index = index % totalcells;

    var size = Vector2 (1.0 / colcount, 1.0 / rowcount);
    var uindex = index % totalcells;
    var vindex = index / totalcells;

    offset = Vector2 ((uindex+colnumber) * size.x, (1.0 - size.y) - (vindex+rownumber) * size.y);
    GetComponent.<Renderer>().material.SetTextureOffset ("_MainTex",offset);
    GetComponent.<Renderer>().material.SetTextureScale ("_MainTex",size);
}