﻿#pragma strict

var talkmessage : String[];
var displaytext : String;
var showtext : boolean;

function OnGUI (){
    if(showtext){
        GUI.Label (Rect(Screen.width/2, Screen.height/2, 500, 100), ""+ displaytext);
    }
}

function Update () {

}
function talk () {
    var other : PlayerStats;
    other = gameObject.Find("Player").GetComponent(PlayerStats);
    other.disablemovement = true;
    showtext = true;
    for(var i=0; i<talkmessage.Length; i++){
        displaytext = talkmessage[i];
        yield WaitForSeconds(2);
    }
    showtext = false;
    other.disablemovement = false;
}